package cn.felord.mvc.excel;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author dax
 * @since 2020/4/5 11:35
 */
@Component
public class DefaultXlsxView extends AbstractXlsxView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, String> userData = (Map<String, String>)model.get("userData");

        Sheet sheet = workbook.createSheet("userDataSheet");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("Name");
        row.createCell(1).setCellValue("Age");

        AtomicInteger num= new AtomicInteger(1);
        userData.forEach((k,v)->{
            Row r = sheet.createRow(num.getAndIncrement());

            r.createCell(0).setCellValue(k);
            r.createCell(1).setCellValue(v);
        });
    }
}
