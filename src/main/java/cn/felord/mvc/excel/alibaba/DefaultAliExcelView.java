package cn.felord.mvc.excel.alibaba;

import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author dax
 * @since 2020/4/8 21:57
 */
@Component
public class DefaultAliExcelView extends AbstractAliExcelView {
    @Override
    protected void doExcelWriter(Map<String, Object> model, ExcelWriterBuilder excelWriterBuilder) {

        Object userData = model.get("userData");
        excelWriterBuilder.sheet("userSheet").doWrite((List) userData);
    }
}
