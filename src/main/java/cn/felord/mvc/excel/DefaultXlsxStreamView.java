package cn.felord.mvc.excel;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.util.Assert;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * @author dax
 * @since 2020/4/5 12:19
 */
public class DefaultXlsxStreamView extends AbstractXlsxStreamingView {
    private BiConsumer<Map<String, Object>, Workbook> dataInjector;

    public void dataInjector(BiConsumer<Map<String, Object>, Workbook> dataInjector) {
        this.dataInjector = dataInjector;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Assert.notNull(dataInjector, "dataInjector must not be null");
        dataInjector.accept(model, workbook);
    }
}
